# Fedora Design Requests

This repository records task and engagements for **the [Fedora Design](https://fedoraproject.org/wiki/Design) team**.

The Fedora Design Team is Fedora's in-house design agency. We provide artwork, user experience, usability, and general design services to the Fedora project. 

## What can I do here?

You can file an [issue](https://gitlab.com/fedora/design/team/requests/-/issues/) to:

* Request new design work
* Tell us if something's wrong with an existing design
* Make a suggestion for a future design 

## I filed an issue, now what?

Because this is mainly a volunteer-staffed team, we don't guarantee to handle all tickets. If you think a ticket needs attention and would like to work on it, but someone else took ownership of the ticket: try to contact the owner and see if they are working on it. If you don't hear from them within a week you can probably take the ticket over. If you have a filed a ticket and have not heard from anyone, please feel free to [https://matrix.to/#/#design:fedoraproject.org](join our  chat) for attention / guidance / support.

## Joining our team / working on issues

If you would like to join the team and contribute as a designer, please complete the steps listed [here](https://fedoraproject.org/wiki/Join_the_Design_Team). (We know these steps are a little out-of-date, and we intend to update them. If you need additional help, [https://matrix.to/#/#design:fedoraproject.org](please join our Matrix-based chat) and talk with us!)

If you are a beginner to this team, please stick with open tickets (with no one assigned) with a "newbie" tag. That means a senior team member has already looked over the ticket and has put some notes in the ticket to help you get started. Untriaged tickets may be invalid or may ask for things that are against our branding or logo guidelines/policies. If you have any questions at any time about any ticket, please ask for help [https://matrix.to/#/#design:fedoraproject.org](in our chat).

## Contacting us

If you want to carry on a longer conversation with the team, you may want to post in the design team area of the Fedora discussions board at [https://discussion.fedoraproject.org/tag/design](discussion.fedoraproject.org/tag/design).

## A note on logos and trademarks

Also, any issues involving the usage of Fedora logos/trademarks must be cognizant and follow [the Fedora Logo Usage guidelines](https://fedoraproject.org/wiki/Logo/UsageGuidelines).
