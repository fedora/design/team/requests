# Design Request

## What do you need?
<!-- In your own words, what are you asking the Fedora Design team for help with? !-->

### Graphic Design / Artwork 
- [ ] A logo design
- [ ] An icon
- [ ] An infographic/chart
- [ ] A template or design for printed materials
- [ ] Swag design
- [ ] Graphic design not covered by the above
 
### User Experience (UX) Design
- [ ] UX analysis / suggestions for improvement
- [ ] User research
- [ ] User testing
- [ ] Application mockups / designs
- [ ] Website mockups / designs
- [ ] Something else UX-related

### Media design
- [ ] Video editing
- [ ] Video titles
- [ ] Animation
- [ ] Something else media design related

## Problem Statement
<!-- What is the issue being faced that this design request will help? !-->

## Contacts
<!-- Who is the primary contact to whom the Fedora Design team can speak with about this issue and deliver the output to? Which Fedora team/project/etc. is this request being made for? !-->

## Deadline
<!-- When do you need this by? If this is for an event, please let us know the date of the event and any lead time you need to get materials produced. !-->

## Who will benefit?
<!-- Will this address a need that only one user has, or will it benefit a lot of people? Which people? !-->

## Inspiration Examples / Competitive Analysis
<!-- Are there any examples of this which exist in other projects? !-->

## Priority/Severity
<!-- Delete as appropriate. The priority and severity assigned may be different to this !-->
- [ ] High (This will bring a huge increase in performance/productivity/usability cover)
- [ ] Medium (This will bring a good increase in performance/productivity/usability)
- [ ] Low (anything else e.g., trivial, minor improvements)


/label ~triage

